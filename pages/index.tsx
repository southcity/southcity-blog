import { GetStaticProps } from 'next'
import React from 'react'
import Card from '../components/card'
import Container from '../components/container'
import Header from '../components/header'
import Layout from '../components/layout'
import { getAllPosts, Post } from '../lib/posts'

export type HomePageProps = {
  posts: Post[]
}

const HomePage: React.FC<HomePageProps> = ({ posts }) => {
  return (
    <Layout>
      <Header>
        <div className="my-2 uppercase leading-tight">
          <h4 className="text-text-dark">My name is</h4>
          <h1>Stian Sørby</h1>
        </div>
        <p className="text-text-dark">Welcome to my development blog</p>
      </Header>

      <Container>
        <div className="flex flex-wrap -m-2">
          {posts.map((post) => (
            <div key={post.slug} className="p-2">
              <Card post={post} />
            </div>
          ))}
        </div>
      </Container>
    </Layout>
  )
}

export default HomePage

export const getStaticProps: GetStaticProps<HomePageProps> = async () => {
  return { props: { posts: getAllPosts() } }
}
