import { GetStaticPaths, GetStaticProps } from 'next'
import React from 'react'
import Markdown from 'react-markdown/with-html'
import Codeblock from '../components/codeblock'
import Container from '../components/container'
import Header from '../components/header'
import Layout from '../components/layout'
import { getPost, getPostSlugs, Post } from '../lib/posts'

export type PostPageProps = {
  post: Post
}

export type PostPageQuery = {
  slug: string
}

const PostPage: React.FC<PostPageProps> = ({ post }) => {
  return (
    <Layout>
      <Header>
        <div className="my-2 uppercase font-bold leading-tight">
          <h1 className="text-4xl">{post.title}</h1>
        </div>
        <p className="text-lg text-text-dark">{post.description}</p>
      </Header>

      <Container>
        <Markdown escapeHtml={false} className="markdown" source={post.content} renderers={{ code: Codeblock }} />
      </Container>
    </Layout>
  )
}

export default PostPage

export const getStaticPaths: GetStaticPaths<PostPageQuery> = async () => {
  return { paths: getPostSlugs().map((slug) => ({ params: { slug } })), fallback: false }
}

export const getStaticProps: GetStaticProps<PostPageProps, PostPageQuery> = async (ctx) => {
  return { props: { post: getPost(ctx.params!.slug) } }
}
