module.exports = {
  purge: ['./components/**/*.{js,ts,jsx,tsx}', './pages/**/*.{js,ts,jsx,tsx}'],
  theme: {
    colors: {
      gray: {
        light: '#313036',
        dark: '#1D1D20',
      },
      white: {
        light: '#EDEDED',
        dark: '#D1D1D1',
      },
      purple: {
        light: '#7F2CCB',
        dark: '#6825A7',
      },
      pink: {
        light: '#E83F6F',
        dark: '#E5245B',
      },
      yellow: {
        light: '#FFBF00',
        dark: '#E0A800',
      },
      green: {
        light: '#81F499',
        dark: '#69F287',
      },
    },
  },
  variants: {},
  plugins: [],
}
