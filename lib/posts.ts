import fs from 'fs'
import matter from 'gray-matter'
import path from 'path'

const __postDir__ = path.join(process.cwd(), 'posts')

export type Post = {
  slug: string
  title: string
  description: string
  tags: string[]
  cover: {
    src: string
    alt: string
  }
  date: string
  content: string
}

export function getPost(slug: string): Post {
  const filePath = path.join(__postDir__, `${slug}.md`)
  const fileContent = fs.readFileSync(filePath, 'utf-8')

  const { data, content } = matter(fileContent)

  return {
    slug,
    title: data.title,
    description: data.description,
    tags: data.tags,
    cover: data.cover,
    date: data.date,
    content,
  } as Post
}

export function getPostSlugs(): string[] {
  return fs.readdirSync(__postDir__).map((file) => file.replace('.md', ''))
}

export function getAllPosts(): Post[] {
  return getPostSlugs()
    .map((slug) => getPost(slug))
    .sort((a, b) => (a.date > b.date ? -1 : 1))
}
