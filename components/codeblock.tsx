import React from 'react'
import { Prism } from 'react-syntax-highlighter'
import { cb } from 'react-syntax-highlighter/dist/cjs/styles/prism'

export type CodeblockProps = {
  language: string
  value: string
}

const Codeblock: React.FC<CodeblockProps> = ({ language, value }) => {
  return (
    <Prism language={language} style={cb}>
      {value}
    </Prism>
  )
}

export default Codeblock
