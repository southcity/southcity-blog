import React from 'react'
import Head from 'next/head'

export type LayoutProps = {}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <Head>
        <title>Blog - Southcity</title>
        <link rel="shortcut icon" type="img/svg" href="/img/logo.svg" />
      </Head>
      {children}
    </>
  )
}

export default Layout
