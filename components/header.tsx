import React from 'react'
import Container from './container'

export type HeaderProps = {}

const Header: React.FC<HeaderProps> = ({ children }) => {
  return (
    <header className="bg-circuit py-4">
      <Container>{children}</Container>
    </header>
  )
}

export default Header
