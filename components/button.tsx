import React from 'react'

export type ButtonProps = {} & React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>

const Button: React.FC<ButtonProps> = ({ children, ...props }) => {
  return (
    <button
      {...props}
      className="px-6 py-2 my-2 bg-purple-light rounded font-semibold duration-100 hover:bg-purple-dark"
    >
      {children}
    </button>
  )
}

export default Button
