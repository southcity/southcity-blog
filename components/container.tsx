import React from 'react'

export type ContainerProps = {}

const Container: React.FC<ContainerProps> = ({ children }) => {
  return <div className="container mx-auto p-6">{children}</div>
}

export default Container
