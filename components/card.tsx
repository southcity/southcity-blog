import { formatDistance, parseISO } from 'date-fns'
import Link from 'next/link'
import React from 'react'
import { Post } from '../lib/posts'
import Button from './button'

export type CardProps = {
  post: Post
}

const Card: React.FC<CardProps> = ({ post }) => {
  return (
    <div className="w-full max-w-sm h-full flex flex-col items-start p-6 bg-gray-light border-l-8 border-purple-light border-solid rounded">
      <img {...post.cover} className="my-2 max-w-full h-32" />
      <h4 className="my-2">{post.title}</h4>
      <p className="text-text-dark mb-2">{post.description}</p>
      <div className="mt-auto">
        <Link href="/[slug]" as={`/${post.slug}`}>
          <a>
            <Button>Read Post</Button>
          </a>
        </Link>
      </div>
      <p className="text-xs mt-2 text-white-dark">posted {formatDistance(parseISO(post.date), new Date())} ago</p>
    </div>
  )
}

export default Card
