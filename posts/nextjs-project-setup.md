---
title: 'My Next.js Project Setup'
description: 'How I bootstrap my Next.js projects using Typescript for static type checking and Tailwindcss for styling'
tags:
  - nextjs
  - tailwindcss
  - typescript
cover:
  src: '/assets/nextjs-project-setup/cover.svg'
  alt: 'Next.js logo'
date: '2020-08-27T00:00:00.228Z'
---

# Introduction

Next.js is a powerful tool for creating SSR (Server Side Rendered) React applications. Here is how I setup my Next.js projects with Typescript for static type checking, and [Tailwindcss](/tailwind-intro) for styling.

---

### Setup The Next.js App

We will start by setting up our Next.js app. For this we will use `create-next-app`. Navigate to the folder you want your project to reside, and run the following command:

```bash
yarn create next-app project-name
# or
npx create-next-app project-name
```

---

### Adding Typescript to the template

Next.js is very flexible and can already provide Typescript support as long as it detects Typescript configuration and files in your project. For Typescript to work in your projects, you have to add 2 files to the project folder, `tsconfig.json` and `next-env.d.ts`. Add the following contents to the 2 files:

```json
// tsconfig.json
{
  "compilerOptions": {
    "allowJs": true,
    "alwaysStrict": true,
    "esModuleInterop": true,
    "forceConsistentCasingInFileNames": true,
    "isolatedModules": true,
    "jsx": "preserve",
    "lib": ["dom", "es2017"],
    "module": "esnext",
    "moduleResolution": "node",
    "noEmit": true,
    "noFallthroughCasesInSwitch": true,
    "noUnusedLocals": true,
    "noUnusedParameters": true,
    "resolveJsonModule": true,
    "skipLibCheck": true,
    "strict": true,
    "target": "esnext"
  },
  "exclude": ["node_modules"],
  "include": ["**/*.ts", "**/*.tsx"]
}
```

```tsx
// next-end.d.ts
/// <reference types="next">
/// <reference types="next/types/global">
```

We will then add the typings and typescript itself to our project dependencies

yarn

```bash
yarn add -D @types/node @types/react @types/react-dom typescript
# or
npm install -D @types/node @types/react @types/react-dom typescript
```

---

### Adding Tailwind

Adding Tailwind to the project will be just as easy. We'll be using postcss for that, and luckily for us, Next.js already comes pre-configured to accept postcss. Start by downloading the necessary dependencies, they will be compiled to regular css, so add them as dev-dependencies.

yarn

```bash
yarn add -D tailwindcss postcss-preset-env
# or
npm install -D tailwindcss postcss-preset-env
```

We will then add the two required config files, `postcss.config.js` and `tailwind.config.js` to the project directory.

```js
// postcss.config.js
module.exports = {
  plugins: ['postcss-preset-env', 'tailwindcss'],
}
```

```js
// tailwind.config.js
module.exports = {
  purge: ['./components/**/*.{js,ts,jsx,tsx}', './pages/**/*.{js,ts,jsx,tsx}'],
  theme: {},
  variants: {},
  plugins: [],
}
```

---

### Conclusion

And there we go, your Next.js project is up and running with Typescript and Tailwindcss. Test out this sample to see it in action:

```tsx
// components/button.tsx
import React from 'react'

export type ButtonProps = {}

const Button: React.FC<ButtonProps> = ({ children }) => {
  return <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">{children}</button>
}

export default Button
```
