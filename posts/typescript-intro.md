---
title: 'Introduction to Typescript'
description: 'Learn how to write and compile Typescript code'
tags:
  - typescript
cover:
  src: '/assets/typescript-intro/cover.svg'
  alt: 'A typewriter'
date: '2020-08-28T10:32:41.228Z'
---

# Introduction

There is no wonder Typescript ranked as the #2 most loved programming language in the [Stack Overflow 2020 Developer Survey](https://insights.stackoverflow.com/survey/2020#technology-most-loved-dreaded-and-wanted-languages-loved). The language is comfortable to write, and provides many advantages such as: great intellisense with most IDEs, compile time error catching, and auto documentation of your code. Compared to it's main alternative; Javascript, you'll quickly learn to see that Typescript often is the far surperior choice.

The Typescript programming language is a superset of Javascript, what does this mean? It means that the language isn't built from scratch, but is rather built on top of Javascript. After you've written your Typescript code, you then compile it to plain Javascript with a binary, and the compiled files can then be ran using Node.js, or whichever Javascript interpreter you wish to use. Without further ado, let's get started.

**Prerequisites:**

- Basic Javascript knowledge
- Node.js
- Yarn or npm

---

# Installation

If you want to follow this tutorial without installing Typescript locally, you can use the [official interactive playground](https://www.typescriptlang.org/play). To install Typescript in your environment make sure you have a Node.js project initialized and a `package.json` in your project root. It is best practice to install typescript on a per-project basis, such that you do not require a machine to have typescript globally installed to edit and compile your project. Run one of the following commands in your terminal depending on which package manager you are using:

```bash
yarn add -D typescript @types/node
# or
npm install -D typescript @types/node
```

This will install the typescript compiler itself, and all the typings that are needed for Node.js, such as those used by for instance the `fs` library, or builtin global variables like `__dirname` and `process`.

---

# Setup

To setup the Typescript compiler we need a config file, which is most often called `tsconfig.json`. You can either create this file manually, or get a scaffolded version with comments explaining each field by running the following command:

```bash
npx tsc --init
```

For now we'll only include the absolute necessary fields, but you can read more about the config file [here](https://aka.ms/tsconfig.json). For that we'll have to specify where Typescript finds our code, where it should place our compiled code, and some other options that make the compiled code Node.js compatible. Add the following content to `tsconfig.json`:

```json
// tsconfig.json
{
  "compilerOptions": {
    "target": "es6",
    "module": "commonjs",
    "rootDir": "./src",
    "outDir": "./dist"
  }
}
```

Lastly we will add a script to our `package.json`, so that anyone can build our code, without knowing that Typescript and it's compiler is used internally. The command for compiling is called `tsc`, and if used in the project directory, it will read our `tsconfig.json` and compile all Typescript files in the `src` directory. Add the following script to `package.json`

```json
{
  "scripts": {
    "start": "node ./dist/index.js",
    "build": "tsc"
  }
}
```

---

# Getting Started

We've specified that Typescript will find our `.ts` files in the `src` directory, so let's start by creating `src/index.ts`. And as tradition goes, we will start with a hello world program, Typescript style.

```ts
const message: string = 'Hello World'

console.log(message)
```

To run it, open your terminal and execute the following command:

```bash
yarn build && yarn start
# or
npm run build && npm start
```
